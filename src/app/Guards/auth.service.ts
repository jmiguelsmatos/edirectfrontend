import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ApiService} from '../shared/api.service';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {
  public logged: BehaviorSubject<boolean> = new BehaviorSubject(false);
  constructor(private apiService: ApiService, private router: Router) { }

  get tokenValid(): Promise<Boolean> {
    const sessionID = window.localStorage.getItem('sessionID');
    const headers = new HttpHeaders()
      .set('sessionID', sessionID);
    return new Promise((resolve, reject) => {
      this.apiService.post('/api/auth/ping', {} , headers).subscribe(data => {
        resolve(data.response);
      },
        (error) => {
          this.router.navigate(['login']);
          resolve(false);
        });
    });
  }

}

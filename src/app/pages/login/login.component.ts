import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(private fb: FormBuilder, private http: HttpClient, public router: Router){}

  ngOnInit(){
    this.loginForm = this.fb.group({
      username: ['', Validators.required ],
      password: ['',Validators.required ]
    });
  }


  onSubmit(){
    if(this.loginForm.valid){
      this.http.post('/api/auth/login', this.loginForm.value).subscribe(data => {
        if(!data['inError']) {
          window.localStorage.setItem('sessionID', data['response']['sessionId']);
          window.localStorage.setItem('userID', data['response']['userId']);
          window.localStorage.setItem('userName', data['response']['username']);
          this.router.navigate(['/dashboard/projects']);
        }else alert('wrong credentials');
      });
    }else alert('form invalid');
  }

}

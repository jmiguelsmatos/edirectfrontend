import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../shared/api.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public users: Object[] = [];
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.get('/api/users').subscribe(data => this.users = data['response']);
  }

  public deleteById(id: string, index: number){
    this.apiService.delete('/api/users/'+id).subscribe(
      data => {
        this.users.splice(index, 1);
      });
  }

}

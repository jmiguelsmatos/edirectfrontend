import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../shared/api.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  public projects: Object[] = [];
  public createProjetForm: FormGroup;
  constructor(private fb: FormBuilder, private apiService: ApiService) { }

  ngOnInit() {
    this.createProjetForm = this.fb.group({
      name: ['', Validators.required ]
    });
    this.getAllProjects();
  }

  public getAllProjects(){
    const userID = window.localStorage.getItem('userID');
    this.apiService.get('/api/project/'+userID).subscribe( data => {
      this.projects = data;
    });
  }


  public onSubmit(){

    const userID = window.localStorage.getItem('userID');
    const newProject = {user_id: userID};

    Object.assign(newProject, this.createProjetForm.value);

    this.apiService.post('/api/project', newProject).subscribe(data => {
      if(!data.response['inError']){
        this.projects.push(data.response);
      }
    });
    this.createProjetForm.reset();
  }

  public removeProject(index: number){
    console.log(index);
    this.projects.splice(index, 1);
  }
}

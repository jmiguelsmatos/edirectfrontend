import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../shared/api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public username: String;

  constructor(private apiService: ApiService, private router: Router) { }

  ngOnInit() {
      this.username = window.localStorage.getItem('userName');
  }

  public logout(){
    this.apiService.post('/api/auth/logout', {}).subscribe(data => this.router.navigate(['/login']));
  }

}

import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public formRegister: FormGroup;

  constructor(private fb: FormBuilder, private http: HttpClient, public router: Router) { }

  ngOnInit() {
    this.formRegister = this.fb.group({
      username: ['', Validators.required ],
      password: ['',Validators.required ],
      address: [''],
      email: ['']
    });
  }

  public onSubmit() {
    this.http.post('/api/users/', this.formRegister.value).subscribe(
      () => {
         this.router.navigate(['/login']);
        },
      (error) => {
        console.log(error);
      });
  }
}

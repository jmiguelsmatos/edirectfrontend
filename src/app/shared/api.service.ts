import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Router} from '@angular/router';


@Injectable()
export class ApiService{

  constructor(private http: HttpClient, private router: Router) {}

  get headers(){
    const sessionID = window.localStorage.getItem('sessionID');
    const headers = new HttpHeaders()
      .set('sessionID', sessionID);
    return headers;
  }

  get(url: string, headers?: any): Observable<any>{
    return this.http.get(url, {headers: this.headers})
      .map((res: Response) => res)
      .catch(this.handleError.bind(this))
      .do(() => {
        console.log('request finished');
      });
  }

  post(url: string, body: any, headers?: any): Observable<any>{
    return this.http.post(url, body, {headers: this.headers})
      .map((res: Response) => res)
      .catch(this.handleError.bind(this))
      .do((res) => {
        console.log(res);
      });
  }

  put(url: string, body: any, headers?: any): Observable<any>{
    return this.http.put(url, body, {headers: this.headers})
      .map((res: Response) => res)
      .catch(this.handleError.bind(this))
      .do(() => {
        console.log('request finished');
      });
  }

  delete(url: string, headers?: any): Observable<any>{
    return this.http.delete(url, {headers: this.headers})
      .map((res: Response) => res)
      .catch(this.handleError.bind(this))
      .do(() => {
        console.log('request finished');
      });
  }

  handleError(error: Response | any): Observable<any>{
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if(error.status===503){
      alert('Session Expired');
      this.router.navigate(['/login']);
    }
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }
}

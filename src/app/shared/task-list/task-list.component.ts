import {Component, Input, OnInit} from '@angular/core';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit{

  @Input('project') project: Object;

  public doneList: Object[];
  public toDoList: Object[];

  public tempIndex: number;
  constructor(private apiService: ApiService) {}

  ngOnInit(){
    this.getLists();
  }


  private getLists() {
    this.toDoList = this.project['tasks'].filter( task => task['state']===0);
    this.doneList = this.project['tasks'].filter( task => task['state']===1);
  }

  public deleteTask(task, index){
    this.apiService.delete('/api/task/' + task['_id']).subscribe(data => {
      this.project['tasks'].splice(index, 1);
      if(task['state']===0){
        this.toDoList.splice(index, 1);
      }else{
        this.doneList.splice(index, 1);
      }
    });
  }

  public addNewTask(inputText){
    const newTask = {project_id: this.project['_id'], name: inputText.value};

    this.apiService.post('/api/task', newTask).subscribe(data => {
      this.project['tasks'].push(data.response);
      this.toDoList.push(data.response);
    });
    inputText.value = '';
  }

  public keyDownFunction(event, inputText) {
    if(event.keyCode === 13) {
      this.addNewTask(inputText);
    }
  }

  public checkBoxState(task, index){
    if(task.state===0){
      task.state=1;
      this.apiService.put('/api/task/'+task._id,task).subscribe(data => {
        this.toDoList.splice(index, 1);
        this.doneList.push(task);
      });
    }else{
      task.state=0;
      this.apiService.put('/api/task/'+task._id,task).subscribe(data => {
        this.doneList.splice(index, 1);
        this.toDoList.push(task);
      });
    }
  }

  public showTooltip(index){
    this.tempIndex = index;
  }
}

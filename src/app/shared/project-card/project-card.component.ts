import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.css']
})
export class ProjectCardComponent {
  @Input('project') project: Object;
  @Input('index') index: Number;

  @Output() deleteProject: EventEmitter<any> = new EventEmitter();

  public editMode = false;

  constructor(private apiService: ApiService) { }


  public removeProject(id: string, index: number){
    this.apiService.delete('/api/project/' + id).subscribe(data => {
      this.deleteProject.emit(index);
    });
  }

  public toggleEditMode() {
    this.editMode = !this.editMode;
  }

  public saveEditMode(id, event, clicked) {
    if(event.keyCode === 13 || clicked) {
      if(clicked) this.project['name'] = event.value; else this.project['name'] = event.target.value;
      this.apiService.put('/api/project/'+ id, this.project).subscribe( data => {
        this.toggleEditMode();
      });
    }
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {LoginComponent} from './pages/login/login.component';
import {RegisterComponent} from './pages/register/register.component';
import {AuthGuard} from './Guards/auth.guard';
import {ProjectsComponent} from './pages/dashboard/projects/projects.component';
import {UsersComponent} from './pages/dashboard/users/users.component';

const routes: Routes = [
    {path:'', redirectTo: 'login' , pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: 'dashboard', component: DashboardComponent, canActivateChild:[AuthGuard],
      children: [
        {path: 'projects' , component: ProjectsComponent},
        {path: 'users' , component: UsersComponent}
      ]
    },
    {path: 'register', component: RegisterComponent},
    {path:'**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

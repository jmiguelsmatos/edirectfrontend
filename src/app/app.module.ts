import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './routing.module';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {LoginComponent} from './pages/login/login.component';
import {RegisterComponent} from './pages/register/register.component';
import {AuthGuard} from './Guards/auth.guard';
import {AuthService} from './Guards/auth.service';
import {ApiService} from './shared/api.service';
import {ProjectsComponent} from './pages/dashboard/projects/projects.component';
import {UsersComponent} from './pages/dashboard/users/users.component';
import {ProjectCardComponent} from './shared/project-card/project-card.component';
import {TaskListComponent} from './shared/task-list/task-list.component';





@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    ProjectsComponent,
    UsersComponent,
    ProjectCardComponent,
    TaskListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [HttpClient, AuthGuard, AuthService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }

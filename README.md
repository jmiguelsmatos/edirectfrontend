# FrontEnd

> eDirect code challenge Frontend 

## Installation

Then clone the project 
```sh
git clone https://jmiguelsmatos@bitbucket.org/jmiguelsmatos/edirectfrontend.git
```

Install it with following command
```sh
npm install
```


## Usage

Ready to go!
Run

```js
npm start
```

Then open your browser in 

http://localhost:4200


## License

MIT � [Miguel Matos]()
